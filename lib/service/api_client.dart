
import 'package:dio/dio.dart';

class ApiClient {
  Dio dio;

  ApiClient(): dio = Dio(
    BaseOptions(
      connectTimeout: 5000,
      receiveTimeout: 5000,
    )
  );

  Dio addInterceptor(Interceptor interceptor) {
    dio.interceptors.add(interceptor);
    return dio;
  }

}