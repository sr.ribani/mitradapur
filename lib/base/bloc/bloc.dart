abstract class Bloc {
  void initState();
  void dispose();
}