import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'bloc/bloc.dart';

abstract class BaseState<T extends StatefulWidget> extends State<T> {

  MediaQueryData _mediaQueryData;
  double screenWidth;
  double screenHeight;

  Bloc get bloc;

  Widget create(BuildContext context);

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return create(context);
  }

}