
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mitradapurapp/base/base_state.dart';
import 'package:mitradapurapp/base/bloc/bloc.dart';

class DashboardPage extends StatefulWidget {
  static const String route = '/dashboard';

  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends BaseState<DashboardPage> {
  var _selectedIndex = 0;

  @override
  Widget create(BuildContext context) {
    return Scaffold(
      body: Center(),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedIndex,
        onTap: (value) => setState(() {
          _selectedIndex = value;
        }),
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.home),
              title: Text('Home')
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.bookmark),
              title: Text('Pesanan')
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.store_mall_directory),
              title: Text('Direktori')
          )
        ],
      ),
    );
  }

  @override
  Bloc get bloc => null;
}