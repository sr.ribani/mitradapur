import 'package:flutter/material.dart';
import 'package:mitradapurapp/injection/global_providers.dart';
import 'package:mitradapurapp/ui/home/home_page.dart';
import 'package:provider/provider.dart';

import 'component/router.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: globalProviders,
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        onGenerateRoute: Router.generateRoute,
        initialRoute: HomePage.route,
      ),
    );
  }
}
