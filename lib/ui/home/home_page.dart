
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mitradapurapp/base/bloc/bloc.dart';
import 'package:mitradapurapp/ui/custom/typed_text_field.dart';
import 'package:mitradapurapp/ui/home/home_bloc.dart';
import 'package:provider/provider.dart';
import '../../base/base_state.dart';

class HomePage extends StatefulWidget {
  static const String route = '/home';

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends BaseState<HomePage> {

  @override
  Widget create(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            title: Text('Mitra Dapurbandung',
              style: GoogleFonts.quicksand(
                fontWeight: FontWeight.bold,
                fontSize: 20,
              ),
            ),
            expandedHeight: 200,
            floating: true,
            pinned: true,
            snap: true,
            flexibleSpace: FlexibleSpaceBar(
                background: SafeArea(
                  child: Container(
                    padding: EdgeInsets.only(top: 70),
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 15),
                      child: Row(
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text('0',
                                style: GoogleFonts.robotoCondensed(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 70,
                                    color: Colors.white
                                ),
                              ),
                              Text('Penjualan',
                                style: GoogleFonts.robotoCondensed(
                                    fontSize: 15,
                                    color: Colors.white
                                ),
                              ),
                            ],
                          ),
                          SizedBox(width: 50),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text('0',
                                style: GoogleFonts.robotoCondensed(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 70,
                                    color: Colors.white
                                ),
                              ),
                              Text('Penghasilan',
                                style: GoogleFonts.robotoCondensed(
                                    fontSize: 15,
                                    color: Colors.white
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                )
            ),
          ),

          new SliverList(
              delegate: new SliverChildListDelegate(_buildList(50))
          ),

        ],

      ),

    );

  }



  List _buildList(int count) {

    List<Widget> listItems = List();
    for (int i = 0; i < count; i++) {
      listItems.add(new Padding(padding: new EdgeInsets.all(20.0),
          child: new Text(
              'Itam ${i.toString()}',
              style: GoogleFonts.quicksand()
          )
      ));
    }
    return listItems;

  }

  @override
  Bloc get bloc => Provider.of<HomeBloc>(context);
}