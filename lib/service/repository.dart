import 'package:mitradapurapp/service/api_service.dart';

class Repository {
  final ApiService apiService;

  Repository(this.apiService);
}