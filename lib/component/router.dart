import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mitradapurapp/service/repository.dart';
import 'package:mitradapurapp/ui/dashboard_page.dart';
import 'package:mitradapurapp/ui/home/home_bloc.dart';
import 'package:mitradapurapp/ui/home/home_page.dart';
import 'package:provider/provider.dart';

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch(settings.name) {
      case HomePage.route :
          return MaterialPageRoute(builder: (_) =>
            ProxyProvider<Repository, HomeBloc>(
              builder: (_, repo, bloc) => HomeBloc(repo),
              child: HomePage(),
            )
          );
      case DashboardPage.route :
          return MaterialPageRoute(builder: (_) => DashboardPage());
    }
  }
}