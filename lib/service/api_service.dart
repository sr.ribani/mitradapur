
import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';

part 'api_service.g.dart';

@RestApi(baseUrl: 'https://newsapi.org/v2')
abstract class ApiService {
  static ApiService create(Dio dio) => _ApiService(dio);

}