import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class TypedTextField extends StatelessWidget {
  final TextEditingController controller;
  final Color fillColor;
  final Widget suffixIcon;
  final String hintText;

  const TypedTextField({
    Key key,
    this.controller,
    this.fillColor,
    this.suffixIcon,
    this.hintText = ''
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: Implement build
    return TextFormField(
      controller: controller,
      decoration: InputDecoration(
        filled: true,
        fillColor: fillColor?? Colors.white,
        suffixIcon: suffixIcon?? Center(),
        hintText: hintText,
        hintStyle: GoogleFonts.roboto(
          fontWeight: FontWeight.normal,
          fontStyle: FontStyle.italic
        ),
        border: OutlineInputBorder(
          borderSide: BorderSide.none
        )
      ),
    );
  }
}