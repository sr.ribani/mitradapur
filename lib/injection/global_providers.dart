import 'package:mitradapurapp/service/api_client.dart';
import 'package:mitradapurapp/service/api_service.dart';
import 'package:mitradapurapp/service/interceptor/logging_interceptor.dart';
import 'package:mitradapurapp/service/repository.dart';
import 'package:provider/provider.dart';

List<SingleChildCloneableWidget> globalProviders = [
  ...independentServices,
  ...dependentServices
];

List<SingleChildCloneableWidget> independentServices = [
  Provider(builder: (_) => ApiClient())
];

List<SingleChildCloneableWidget> dependentServices = [
  ProxyProvider<ApiClient, ApiService>(
    builder: (_, client, service) => ApiService.create(
      client.addInterceptor(LoggingInterceptors())
    ),
  ),
  ProxyProvider<ApiService, Repository>(
    builder: (_, service, repository) => Repository(service),
  )
];